import {
  Chart as ChartJS, ChartConfiguration, ChartData, ChartDataSets, ChartLegendOptions, ChartType } from 'chart.js';
import { Component, h } from 'preact';

export interface IChartProps {
  data: ChartData;
  getDatasetAtEvent?: DatasetFn;
  getElementAtEvent?: (el: HTMLElement, event: MouseEvent) => void;
  getElementsAtEvent?: (elements: HTMLElement[], event: MouseEvent) => void;
  height?: number;
  id?: string;
  keyProvider?: (dataset: ChartDataSets) => string;
  legend?: ChartLegendOptions;
  onElementsClick?: (element: HTMLElement, ev: MouseEvent) => void;
  options?: ChartConfiguration['options'];
  plugins?: ChartPlugin[];
  redraw?: boolean;
  type: ChartType;
  width?: number;
}

export type ChartDataFn = () => ChartData;
export type DatasetFn = (dataset: ChartDataSets[], e: Event) => void;
export type ChartPlugin = typeof ChartJS['plugins'];

export class Chart extends Component<IChartProps, any> {
  public static keyProvider: IChartProps['keyProvider'] = (d) => d.label || '';

  private chart: ChartJS;

  private element: CanvasRenderingContext2D | HTMLCanvasElement;

  private ref: (el: CanvasRenderingContext2D | HTMLCanvasElement) => void;

  private onClickHandle: (ev: MouseEvent) => void;

  public constructor(props: IChartProps, state: any) {
    super(props, state);

    this.ref = (el: CanvasRenderingContext2D | HTMLCanvasElement) => {
      this.element = el;
    };

    this.onClickHandle = (event: MouseEvent) => {
      if (this.props.getDatasetAtEvent) {
        this.props.getDatasetAtEvent(this.chart.getDatasetAtEvent(event), event);
      }

      if (this.props.getElementAtEvent) {
        this.props.getElementAtEvent(this.chart.getElementAtEvent(event), event);
      }

      if (this.props.getElementsAtEvent) {
        this.props.getElementsAtEvent(this.chart.getElementsAtEvent(event), event);
      }

      if (this.props.onElementsClick) {
        this.props.onElementsClick(this.chart.getElementAtEvent(event), event);
      }
    };
  }

  public componentDidMount() {
    // Render the chart once it's parent container was mounted
    this.renderChart();
  }

  public componentWillUnmount() {
    // Destroy the chart if it's parent container is being removed
    this.chart.destroy();
  }

  public componentDidUpdate() {
    // Make sure to recreate the chart if redraw mode is enabled
    if (this.props.redraw) {
      this.chart.destroy();
      this.renderChart();
      return;
    }
  }

  public render() {
    return <canvas
      ref={this.ref}
      height={this.props.height || 150}
      width={this.props.width || 300}
      onClick={this.onClickHandle}
      {...this.props.id ? { id: this.props.id } : {} } />;
  }

  private renderChart() {
    this.chart = new ChartJS(this.element, {
      data: this.props.data,
      options: {
        // Enforce defaults
        legend: {
          display: true,
          position: 'bottom',
        },

        // Ovewrite with specified
        ...this.props.options || {},
      },
      plugins: this.props.plugins as any[] || [],
      type: this.props.type,
    });
  }
}
