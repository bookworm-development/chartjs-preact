import { Component, h } from 'preact';

import { Chart, IChartProps } from '../chart';
import { Omit } from '../omit';

export class Scatter extends Component<Omit<IChartProps, 'type'>, any> {
  public render() {
    return <Chart
      {...this.props}
      type='scatter' />;
  }
}
